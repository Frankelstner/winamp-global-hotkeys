A barebones plugin (.dll) for Winamp that enables global hotkeys. The keys are all hardcoded. There are other plugins that achieve similar things and are easier to configure. Winamp itself has a plugin for global hotkeys but no mouse wheel support. (And of course, Autohotkey is always an option.) Before creating this, I had used shortcutter 1b5 (all download links are dead apparently) which does support the mouse wheel. This plugin here differs in several ways however:

* When using the mouse wheel, I require two modifiers instead of one. With the shortcutter, I would occasionally scroll through songs or change volume in games simply because I scrolled while sprinting or crouching.
* I use scancodes instead of virtual keys. Basically, scancode+locale = virtual key. The virtual key is influenced by the keyboard locale, which means that when I switch between English and German keyboard locale, y and z are now swapped and I need to hit different physical keys. Scancodes are locale independent, but apparently depend on the keyboard type. The hardcoded scancodes should be correct for USB ISO keyboards.
* I sometimes have long audio files and just want to loop a certain part. This plugin here has hotkeys to set the start and end time and to enable/disable the loop.
* Windows hooks are troublesome, so I use the interception API. E.g. the normal mouse hook does not work in games and the low level mouse hook is occasionally silently removed by Windows; additionally they are not registered when the active window is run in admin mode when Winamp is not. The Windows raw mouse input allows me to receive input, but not block it from reaching the original window. Interception does not have any of these issues.


## Default keys (the keys \zxc below correspond to English locale on my ISO keyboard; actual keys may differ)

You will most likely want to change the keys. Or definitely turn off the Alt+Shift hotkey in the Windows options that changes locale. It is pointless anyway because Win+space does the same.

### Mouse wheel:

* Alt+mouse thumb button 1: Change volume.
* Alt+mouse thumb button 2: Seek through a song.
* Alt+F5 (that is my mouse thumb button 3): Scroll through the playlist.
 
### General keys (Alt+Shift+...):

* \: Pause/Unpause. 
* z: Previous track.
* x: Pause/Unpause.
* c: Next track.

The idea of the \ and x locations is a quick access to pause without removing the middle finger from W.

Additionally, alt+shift+arrow keys for volume and seek. 

### Loop keys (Ctrl+Alt+...):

* \: Toggle loop. After setting start time and/or end time, press this to activate or deactivate the loop.
* z: Set current track location as the loop start time. If no end time is given, the end of track is used. This also sets the current track to repeat to avoid trouble when Winamp manages to play until the end and loads the next track. 
* x: Set current track location as the loop end time. If no start time is given, the start of track is used. This also sets the current track to repeat.
* c: Reset loop. This removes all loop info and disables the current track repeat.

Additionally, ctrl+alt+arrow keys for volume and seek.

## Installation

Get interception from http://www.oblita.com/interception.html and install it. You must run cmd as admin for that and cd to the right place.
If you don't want to change any keys (or anything else): Move the dll into the plugins folder of Winamp. Otherwise, you must compile on your own.

### Compilation

The files should work fine when plugged into an empty dll project. Remember to compile to x86 because Winamp is x86.

Alternatively, open the .sln in Visual Studio. Make sure that the dropdown menus at the top say "Release" and "x86". There is one compiler setting that will likely cause trouble. Go to Debug -> gen_globalhotkeys Properties... (at the bottom). Make sure that (in this new window) the dropdown menus at the top say "Release" and "x86". Choose General (at the top) and select a Platform Toolset that you have installed. 

Press Ctrl+Shift+B to compile. The project is configured so that Visual Studio will automatically try to copy the dll to C:\Program Files (x86)\Winamp\Plugins, but it needs admin rights; run Visual Studio with admin rights if you want this automatic copy. Even then however, the copy fails if the dll already exists and is in use by Winamp. So remember to close that.

If you want to figure out scancodes, the best way is to use any Autohotkey script with a keyboard hook and double click on its tray icon, then press Ctrl+K and read the SC column; this gives back the numbers in hex. Recall that programming languages usually support the prefix 0x to write such numbers, so for a programming language, 0x01E is the same as 0x1E is the same as 30.