#include <string>
#include <vector>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "interception/library/interception.h"

// Grab messages from the SDK headers to not worry about includes.
#define IPC_ISPLAYING 104
#define IPC_GETOUTPUTTIME       105
#define IPC_JUMPTOTIME          106
#define IPC_SET_MANUALPLADVANCE 635
#define IPC_SETVOLUME           122
#define WINAMP_BUTTON1          40044
#define WINAMP_BUTTON2          40045
#define WINAMP_BUTTON3          40046
#define WINAMP_BUTTON4          40047
#define WINAMP_BUTTON5          40048
#define WINAMP_VOLUMEUP         40058
#define WINAMP_VOLUMEDOWN       40059
#define WINAMP_FFWD5S           40060
#define WINAMP_REW5S            40061

#define GPPHDR_VER 0x10  // Version of the winampGeneralPurposePlugin (GPP) structure.
#define PLUGIN_NAME "Global hotkeys"

// Callback functions/events that will be called by Winamp:
int  init(void);
void config(void);
void quit(void);


using namespace std;

// This struct is exposed to Winamp and gives access to the three functions init, config, quit.
typedef struct {
	int version;                // Version of the plugin.
	const char* description;    // Name of the plugin.
	int(*init)();               // Function which will be executed on init event.
	void(*config)();            // Function which will be executed on config event.
	void(*quit)();              // Function which will be executed on quit event.
	HWND hwndParent;            // Handle to Winamp main window, filled out by Winamp when this dll is loaded.
	HINSTANCE hDllInstance;     // hinstance to this dll, filled out by Winamp when this dll is loaded.
} winampGeneralPurposePlugin;

// Global struct to be passed to Winamp.
winampGeneralPurposePlugin plugin = { GPPHDR_VER, PLUGIN_NAME, init, config, quit, 0, 0 };

// This is the only thing directly exposed to Winamp. Winamp calls it and fills out hwndParent and hDllInstance here.
// It then calls init.
extern "C" __declspec(dllexport) winampGeneralPurposePlugin* winampGetGeneralPurposePlugin() {
	return &plugin;
}

// Threads to receive inputs and to loop song parts.
HANDLE inputLoop;
HANDLE songLoop;
InterceptionContext context;

HWND winamp;               // Shared handle to Winamp so I can send messages.
int leftTime = 0;          // Loop start time in ms.
int rightTime = 0;         // Loop end time in ms.
int hasLeft = 0;           // If 0 and the user defines right, set leftTime = 0.
int hasRight = 0;          // If 0 and the user defines left, set righttime = end of track.
int isLoopingRightNow = 0; // Whether to loop or not.
int songLoopShallLive = 1; // Signal to kill the thread.


DWORD WINAPI SongLoop(LPVOID lpParam) {
	// Responsible for looping a song slice.
	// This runs in its own thread and checks every 200 ms if something should be looped.
	while (songLoopShallLive) {
		if (isLoopingRightNow) {
			int curTime = SendMessage(winamp, WM_USER, 0, IPC_GETOUTPUTTIME);
			int timeUntilLoop = rightTime - curTime;

			if (timeUntilLoop <= 0)  // Loop is overdue. Loop it.
				PostMessage(winamp, WM_USER, leftTime, IPC_JUMPTOTIME);
			else if (timeUntilLoop < 200) {  // Loop is imminent, so sleep by that amount.
				Sleep(timeUntilLoop);
				continue; // Make sure that I still want to loop.
			}
			else if (curTime < leftTime - 2000) { // I am more than 2 s left of the loop start.
				// If this is within one loop range, start at the right and seek backwards. 
				// Else go to leftTime. (I could modulo here, but the functionality will not be useful.)
				int distance = leftTime - curTime;
				if (distance < rightTime - leftTime)
					PostMessage(winamp, WM_USER, rightTime - distance, IPC_JUMPTOTIME);
				else
					PostMessage(winamp, WM_USER, leftTime, IPC_JUMPTOTIME);
			}
			else // Loop is active but there is no imminent event.
				Sleep(200);
		}
		else // Loop is not active.
			Sleep(200);
	}
	return 0;
}


#define SC_CONTROL 29
#define SC_SHIFT 42
#define SC_ALT 56
#define SC_BACKSLASH 86
#define SC_LEFT 75
#define SC_RIGHT 77
#define SC_UP 72
#define SC_DOWN 80
#define SC_F5 63


void inline ChangeVolume(int steps) {
	int curVolume = SendMessage(winamp, WM_USER, -666, IPC_SETVOLUME);
	int target = curVolume + steps;
	target = max(0, min(255, target)); // Winamp volume is a value from 0-255.
	PostMessage(winamp, WM_USER, target, IPC_SETVOLUME);
}

// Pressing a hotkey shall inform Winamp and block the input from reaching other windows.
// In particular, mouse thumb button releases must be intercepted because it is the release that usually causes the action.
// alt+thumb+wheel should not send out thumb inputs, but I cannot know beforehand if the user wants to use the wheel. 
// => alt+thumb should not send thumb inputs. This is still fairly intuitive. Pressing modifiers alters the input.
// 
// Key release order is an issue:
//   alt down -> thumb down -> thumb up -> alt up    => The thumb up sees that alt is pressed and knows that it is in hotkey mode; so I can block it.
//   alt down -> thumb down -> alt up -> thumb up    => The thumb does not see anything. Blocking it is not as simple.
//
// => Set the flag if thumb is pressed during alt. Or when a pure keyboard hotkey is detected.
// The flag says that we are in hotkey mode. It is released when alt+thumb are both released.
//
//
// When a hotkey is detected, set a flag.
//
// While the flag is set, intercept thumb release input:
//	 I could also check stray keyboard releases, but they do not really matter and intercepting that would just bloat the code.
//   (I would need to write an intercept switch that is just a copy of the original switch, but only decides whether that key release shall be intercepted.)
// While the flag is set, send a fake key release on alt release:
//	 This stops the alt menu from appearing when no key was pressed between alt press+release.
//   Because I intercept the hotkey keys+buttons, they are never seen and alt will open the menu without this fake input.
//
// Remove the flag only when alt and thumb are released:
//   When key release is alt and no thumb is not pressed, remove the flag.
//   When release is thumb and alt is not pressed, remove the flag.



DWORD WINAPI InputLoop(LPVOID lpParam) {
	// Responsible for receiving user input.

	// Change priority because the interception examples do this too.
	Sleep(500); // I cannot change Winamp priority without waiting. Trial and error.
	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);

	InterceptionDevice device;
	InterceptionStroke stroke;
	context = interception_create_context();
	// My keyboard for whatever reason occasionally adds a higher bit, but only on some bootups and only for some keys, so must filter all keyboard inputs and then later on do &0xff.
	interception_set_filter(context, interception_is_keyboard, INTERCEPTION_FILTER_KEY_ALL); //INTERCEPTION_FILTER_KEY_DOWN | INTERCEPTION_FILTER_KEY_UP);
	interception_set_filter(context, interception_is_mouse, INTERCEPTION_FILTER_MOUSE_BUTTON_4_DOWN | INTERCEPTION_FILTER_MOUSE_BUTTON_4_UP |
		INTERCEPTION_FILTER_MOUSE_BUTTON_5_DOWN | INTERCEPTION_FILTER_MOUSE_BUTTON_5_UP | INTERCEPTION_FILTER_MOUSE_WHEEL);

	unsigned char keyStates[128]; // Track 128 keyboard keys. 1 = released, 0 = pressed.
	for (int i = 0;i < 128;i++) keyStates[i] = 1;  // All keys initialized to release.

	// Thumb buttons. They cannot be mapped trivially so I might as well keep them here.
	int frontPressed = 0;
	int backPressed = 0;

	int interceptRelease = 0;  // Whether a hotkey was pressed and alt+thumb have not been yet released.

	while (interception_receive(context, device = interception_wait(context), &stroke))
	{
		auto kstroke = (KEYBOARD_INPUT_DATA*)stroke;
		auto mstroke = (MOUSE_INPUT_DATA*)stroke;

		if (interception_is_keyboard(device)) {
			kstroke->MakeCode &= 0xff; // Remove potential higher bit added by the keyboard on some bootups and for some keys.

			if (kstroke->MakeCode >= 128) {
				interception_send(context, device, &stroke); continue;}


			if (interceptRelease && kstroke->MakeCode == SC_ALT && (kstroke->Flags & 1)) {
				// Alt release after a hotkey was pressed. Send a fake key release to ensure that the alt menu does not open.
				auto oldCode = kstroke->MakeCode;
				kstroke->MakeCode = 123;   // No idea what key that is, but its release should never cause trouble.
				interception_send(context, device, &stroke);
				kstroke->MakeCode = oldCode;
				keyStates[kstroke->MakeCode] = kstroke->Flags & 1;  // This block will leave early so set the alt release here.
				interception_send(context, device, &stroke);

				if (keyStates[SC_F5] && !frontPressed && !backPressed)  // No thumb button pressed.
					interceptRelease = 0;
				continue;
			}

			// If alt down and alt down is pressed, intercept.
			if (!keyStates[SC_ALT] && kstroke->MakeCode == SC_ALT && !(kstroke->Flags & 1))
				continue;

			keyStates[kstroke->MakeCode] = kstroke->Flags & 1;

			// Pass on key releases even if the key press was intercepted. 
			// Stray key releases are not an issue and this helps me to return to the safe state (no keys pressed) asap.
			if (kstroke->Flags & 1) {
				interception_send(context, device, &stroke);
				continue;
			}

			// Intercept F5 mouse thumb button press. Release needs no special handling because this is keyboard input after all.
			if (kstroke->MakeCode == SC_F5 && !keyStates[SC_ALT]) {
				interceptRelease = 1;
				continue;
			}
		}
		else if (!mstroke->ButtonData) {
			// User has pressed a button (ButtonFlags), instead of using the mouse wheel (ButtonData).
			auto buttons = mstroke->ButtonFlags;
			auto backState = buttons>>6 & 3; // state can be 0, 1 or 2. 0 means unchanged, 1 means press, 2 means release.
			auto frontState = buttons>>8 & 3;
			if (backState)
				backPressed = !(backState - 1);
			else
				frontPressed = !(frontState - 1);

			auto hasPressed = (backState | frontState) == 1;
			if (hasPressed) {
				if (!keyStates[SC_ALT]) {
					interceptRelease = 1;
					continue;  // Intercept thumb button press during alt.
				}
			}
			else if (interceptRelease) {
				if (keyStates[SC_ALT])
					interceptRelease = 0;  // Stop interception because alt is not pressed and thumb button just released.
				continue;  // Button released during interception. Do not pass.
			}
		}
		
		if (keyStates[SC_ALT]) { // Alt is not pressed right now. All hotkeys need alt.
			interception_send(context, device, &stroke);
			continue;
		}

		if (interception_is_keyboard(device)) {
			if (!keyStates[SC_SHIFT]) {
				// I must set interceptRelease here. Imagine input order: shift down -> alt down -> x down (blocked) -> alt up.
				// The alt down -> alt up sequence is registered.
				switch (kstroke->MakeCode) {
				case 44: {  // z    Previous song.
					PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON1, 0); interceptRelease = 1; continue;}
				case 86:   // \   
				case 45: {  // x    Start/Stop. Play.
					if (SendMessage(winamp, WM_USER, 0, IPC_ISPLAYING)==0)
						PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON2, 0);
					else
						PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON3, 0);
					interceptRelease = 1;
					continue;
				}
				case 46: {  // c    Next song.
					PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON5, 0); interceptRelease = 1; continue;}
				case 72: {  // up    Volume up.
					ChangeVolume(8); interceptRelease = 1; continue;}
				case 80: {  // down    Volume down.
					ChangeVolume(-8); interceptRelease = 1; continue;}
				case 75: {  // left    5 s left.
					PostMessage(winamp, WM_COMMAND, WINAMP_REW5S, 0); interceptRelease = 1; continue;}
				case 77: {  // right   5 s right.
					PostMessage(winamp, WM_COMMAND, WINAMP_FFWD5S, 0); interceptRelease = 1; continue;}
				default: {  // Unwanted keyboard input.
					interception_send(context, device, &stroke); continue;}
				}
			}
			else if (!keyStates[SC_CONTROL]) {
				// I do not need to intercept anywhere here because alt release has no effect when control is pressed.
				switch (kstroke->MakeCode) {
				case 86: {  // \    SongLoop: Activate/Deactivate the loop.
					if (!isLoopingRightNow) {
						if (!hasLeft && !hasRight)
							continue;  // User wants to loop but has not set up anything. Ignore the input.

						// Loop wanted and I have info to loop. Seek the start immediately.
						PostMessage(winamp, WM_USER, leftTime, IPC_JUMPTOTIME);
					}
					isLoopingRightNow = !isLoopingRightNow;
					continue;}
				case 44: {  // z    SongLoop: Store left.
					leftTime = SendMessage(winamp, WM_USER, 0, IPC_GETOUTPUTTIME);
					int songLength = SendMessage(winamp, WM_USER, 2, IPC_GETOUTPUTTIME);
					hasLeft = TRUE;
					if (!hasRight)
						rightTime = songLength - 2500;  // Cut off the last 2.5 s because Winamp schedules its loop in advance and may ignore my loop message. Even with 2.5 s, Winamp eats a lot of CPU when looping here.

					leftTime = min(leftTime, songLength - 2800);  // Edge case where leftTime is at the song end.
					rightTime = max(leftTime + 300, rightTime);   // Force rightTime to come at least 300 ms later.
					PostMessage(winamp, WM_USER, 1, IPC_SET_MANUALPLADVANCE);  // Loop track so Winamp cannot escape.
					continue;} 
				case 45: {  // x    SongLoop: Store right.
					int songLength = SendMessage(winamp, WM_USER, 2, IPC_GETOUTPUTTIME);
					rightTime = min(SendMessage(winamp, WM_USER, 0, IPC_GETOUTPUTTIME), songLength - 2500);
					hasRight = TRUE;
					if (!hasLeft)
						leftTime = 0;

					rightTime = max(rightTime, 300);  // Edge case where rightTime is at the song start.
					leftTime = min(rightTime - 300, leftTime);  // Force leftTime to come at least 300 ms before rightTime.
					PostMessage(winamp, WM_USER, 1, IPC_SET_MANUALPLADVANCE);  // Loop track so Winamp cannot escape.
					continue;}
				case 46: {  // c   SongLoop: Reset everything.
					hasLeft = FALSE;
					hasRight = FALSE;
					isLoopingRightNow = FALSE;
					PostMessage(winamp, WM_USER, 0, IPC_SET_MANUALPLADVANCE);  // Undo "Loop track" so Winamp goes to the next song as usual.
					continue;}
				case 72: {  // up    Volume up.
					ChangeVolume(8); continue;}
				case 80: {  // down    Volume down.
					ChangeVolume(-8); continue;}
				case 75: {  // left    5 s left.
					PostMessage(winamp, WM_COMMAND, WINAMP_REW5S, 0); continue;}
				case 77: {  // right   5 s right.
					PostMessage(winamp, WM_COMMAND, WINAMP_FFWD5S, 0); continue;}
				default: {
					interception_send(context, device, &stroke); continue;}
				}
			}
			else // Unwanted keyboard input. No second modifier given.
				interception_send(context, device, &stroke);

		}
		else {
			short wheel = mstroke->ButtonData;  // Multiple of 120 for reasons. 120 equals one input unit.

			if (frontPressed)
				ChangeVolume(wheel / 15);  // 8 steps per 120 mouse wheel units.
			else if (backPressed) {
				while (wheel < 0) {
					PostMessage(winamp, WM_COMMAND, WINAMP_REW5S, 0);
					wheel += 120;
				}
				while (wheel > 0) {
					PostMessage(winamp, WM_COMMAND, WINAMP_FFWD5S, 0);
					wheel -= 120;
				}
			}
			else if (!keyStates[SC_F5]) {
				while (wheel < 0) {
					PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON1, 0);
					wheel += 120;
				}
				while (wheel > 0) {
					PostMessage(winamp, WM_COMMAND, WINAMP_BUTTON5, 0);
					wheel -= 120;
				}
			}
			else  // Unwanted mouse wheel input.
				interception_send(context, device, &stroke);
		}
	}
	interception_destroy_context(context);
	return 0;
}


int init() {
	// Called when Winamp starts up, right after it fills out the plugin struct.
	winamp = plugin.hwndParent;
	inputLoop = CreateThread(0, 0, InputLoop, 0, 0, 0);
	songLoop = CreateThread(0, 0, SongLoop, 0, 0, 0);
	return 0;
}

void config() {
	// Called when the user wants to configure the plugin in the options.
}
void quit() {
	// Called when Winamp exits.
	interception_destroy_context(context); // Close device handles.
	CloseHandle(inputLoop);
	songLoopShallLive = 0;
	CloseHandle(songLoop);
}
