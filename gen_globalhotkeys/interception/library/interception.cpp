#include <stdio.h>
#include <windows.h>
#include <winioctl.h>
#include "interception.h"
#include <string>

#define IOCTL_SET_PRECEDENCE    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GET_PRECEDENCE    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_SET_FILTER		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x804, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GET_FILTER		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x808, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_SET_EVENT         CTL_CODE(FILE_DEVICE_UNKNOWN, 0x810, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_WRITE             CTL_CODE(FILE_DEVICE_UNKNOWN, 0x820, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_READ              CTL_CODE(FILE_DEVICE_UNKNOWN, 0x840, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GET_HARDWARE_ID   CTL_CODE(FILE_DEVICE_UNKNOWN, 0x880, METHOD_BUFFERED, FILE_ANY_ACCESS)


HANDLE* interception_create_context() {
	// If any handle or event cannot be created, shut down everything and return 0.
	char device_name[] = "\\\\.\\interception00";
	DWORD bytes_returned;
	HANDLE* handles = (HANDLE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, INTERCEPTION_MAX_DEVICE * sizeof(HANDLE) * 2);
	if(!handles) return 0;

	auto events = handles + INTERCEPTION_MAX_DEVICE;  // Everything is allocated on handles, but events is the second half of that allocated memory.
	for(int i = 0; i < INTERCEPTION_MAX_DEVICE; ++i)
	{
		if (i < 10)
			device_name[17] = '0' + i;
		else {
			device_name[17] = '0' + (i - 10);
			device_name[16] = '1';
		}

		//sprintf(&device_name[sizeof(device_name) - 3], "%02d", i);   // Modify the string to become e.g. "\\\\.\\interception13"

		handles[i] = CreateFileA(device_name, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
		if (handles[i] == INVALID_HANDLE_VALUE)
			{interception_destroy_context(handles); return 0;}

		events[i] = CreateEventA(NULL, TRUE, FALSE, NULL);  // Register event for this device. Yes, the event is called unempty.
		if(events[i] == NULL)
			{interception_destroy_context(handles); return 0;}

		HANDLE zero_padded_event[2] = {};
		zero_padded_event[0] = events[i];  // Event handle with some 4 nulls at the end.
		if(!DeviceIoControl(handles[i], IOCTL_SET_EVENT, zero_padded_event, sizeof(zero_padded_event), NULL, 0, &bytes_returned, NULL))
			{interception_destroy_context(handles); return 0;}
	}
	return handles;
}

void interception_destroy_context(HANDLE* handles) {
	if (handles) {
		auto events = handles + INTERCEPTION_MAX_DEVICE;
		for (int i = 0; i < INTERCEPTION_MAX_DEVICE; ++i) {
			if (handles[i] != INVALID_HANDLE_VALUE) CloseHandle(handles[i]);
			if (events[i] != NULL)                  CloseHandle(events[i]);
		}
		HeapFree(GetProcessHeap(), 0, handles);
	}
}

void interception_set_filter(HANDLE* handles, InterceptionPredicate interception_predicate, InterceptionFilter filter) {
	// Send a message to the right devices to filter on certain events.
	// E.g. if interception_is_keyboard, only the first 10 devices are let through here. 
	// The filters overlap between keyboard and mouse, i.e. the function of keyboard or mouse is hardcoded into the device.
	// I cannot change device 2 into a mouse or device 12 into a keyboard.
	DWORD bytes_returned;
	if(handles)
		for(int i = 0; i < INTERCEPTION_MAX_DEVICE; ++i)
			if(handles[i] && interception_predicate(i))  
				DeviceIoControl(handles[i], IOCTL_SET_FILTER, (LPVOID)&filter, sizeof(InterceptionFilter), NULL, 0, &bytes_returned, NULL);
}

InterceptionDevice interception_wait(HANDLE* handles) {
	return interception_wait_with_timeout(handles, INFINITE);
}

inline InterceptionDevice interception_wait_with_timeout(HANDLE* handles, unsigned long milliseconds) {
	if(!handles) return 0;
	auto events = handles + INTERCEPTION_MAX_DEVICE;
	DWORD k = WaitForMultipleObjects(INTERCEPTION_MAX_DEVICE, events, FALSE, milliseconds);
	if(k == WAIT_FAILED || k == WAIT_TIMEOUT) return -1;
	return k;
}

void interception_send(HANDLE* handles, InterceptionDevice device, InterceptionStroke* stroke) {
	DWORD strokeswritten;
	if(!handles)
		return;
	auto size = interception_is_keyboard(device) ? sizeof(KEYBOARD_INPUT_DATA) : sizeof(MOUSE_INPUT_DATA);
	DeviceIoControl(handles[device], IOCTL_WRITE, stroke, size, NULL, 0, &strokeswritten, NULL);
}

int interception_receive(HANDLE* handles, InterceptionDevice device, InterceptionStroke* stroke) {
	DWORD strokesread;
	if (!handles || (interception_is_invalid(device)))
		return 0;
	auto size = interception_is_keyboard(device) ? sizeof(KEYBOARD_INPUT_DATA) : sizeof(MOUSE_INPUT_DATA);
	DeviceIoControl(handles[device], IOCTL_READ, NULL, 0, stroke, size, &strokesread, NULL);
	return 1;
}

int interception_is_invalid(InterceptionDevice device) {
	return device < 0 || device >= INTERCEPTION_MAX_DEVICE;
}
int interception_is_keyboard(InterceptionDevice device) {
	//return device >= 0 && device < INTERCEPTION_MAX_KEYBOARD;
	return device < INTERCEPTION_MAX_KEYBOARD;
}
int interception_is_mouse(InterceptionDevice device) {
	//return device >= INTERCEPTION_MAX_KEYBOARD && device < INTERCEPTION_MAX_DEVICE;
	return device >= INTERCEPTION_MAX_KEYBOARD;
}
